package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"whatsapp-service-ocin/helper"
)

func Login(c *gin.Context) {
	var err error

	wai := helper.NewWaInstance()
	if err = wai.Connect(); err != nil {
		helper.ResponseError(c, helper.ErrorWAConnectionMsg, err)
		return
	}

	err = wai.Login()
	if err != nil {
		helper.ResponseError(c, "gagal login", err)
		return
	}

	helper.ResponseSuccess(c, "sukses login", nil)
}

func SendMessage(c *gin.Context) {
	var msgId string
	var err error

	waNumber := c.PostForm("wa_number")
	message := c.PostForm("message")

	wai := helper.NewWaInstance()
	if err = wai.Connect(); err != nil {
		helper.ResponseError(c, helper.ErrorWAConnectionMsg, err)
		return
	}

	msgId, err = wai.SendMessage(waNumber, message)
	if err != nil {
		helper.ResponseError(c, "gagal mengirim pesan", nil)
		return
	}

	helper.ResponseSuccess(c, "sukses mengirim pesan", map[string]interface{}{
		"msg_id": msgId,
	})
}

func SendInvoice(c *gin.Context) {
	var msgId string
	var err error

	waNumber := c.PostForm("wa_number")
	message := c.PostForm("message")

	wai := helper.NewWaInstance()
	if err = wai.Connect(); err != nil {
		helper.ResponseError(c, helper.ErrorWAConnectionMsg, err)
		return
	}

	msgId, err = wai.SendInvoiceMessage(waNumber, message)
	if err != nil {
		helper.ResponseError(c, "gagal mengirim pesan", err)
		return
	}

	helper.ResponseSuccess(c, "sukses mengirim pesan", map[string]interface{}{
		"msg_id": msgId,
	})
}

func SendToInventioGroup(c *gin.Context) {
	var msgId string
	var err error

	message := c.PostForm("message")

	wai := helper.NewWaInstance()
	if err = wai.Connect(); err != nil {
		helper.ResponseError(c, helper.ErrorWAConnectionMsg, err)
		return
	}

	msgId, err = wai.SendGroupMessage(helper.InventioGroupId, message)
	if err != nil {
		helper.ResponseError(c, "gagal mengirim pesan", err)
		return
	}

	helper.ResponseSuccess(c, "sukses mengirim pesan", map[string]interface{}{
		"msg_id": msgId,
	})
}

func GetQrCodeUrl(c *gin.Context) {
	wai := helper.NewWaInstance()
	helper.ResponseSuccess(c, "sukses mendapatkan url qr code", wai.GetQrCode())
	return
}

func TestConnection(c *gin.Context) {
	wai := helper.NewWaInstance()
	if err := wai.Connect(); err != nil {
		helper.ResponseError(c, helper.ErrorWAConnectionMsg, nil)
		return
	}

	connect, err := wai.TestConnection()
	if err != nil {
		helper.ResponseError(c, "gagal melakukan tes koneksi", nil)
		return
	}
	if !connect {
		helper.ResponseError(c, "koneksi terputus", nil)
		return
	}
	helper.ResponseSuccess(c, "koneksi tersambung", nil)
}

func GetInfoWhatsapp(c *gin.Context) {
	wai := helper.NewWaInstance()
	if err := wai.Connect(); err != nil {
		helper.ResponseError(c, helper.ErrorWAConnectionMsg, nil)
		return
	}

	data, err := wai.GetInfoWhatsapp()
	if err != nil {
		helper.ResponseError(c, "gagal mendapatkan info", err)
		return
	}

	helper.ResponseSuccess(c, "sukses mendapatkan info", data)
}

func SearchInWhatsapp(c *gin.Context) {
	wai := helper.NewWaInstance()
	if err := wai.Connect(); err != nil {
		helper.ResponseError(c, helper.ErrorWAConnectionMsg, err)
		return
	}

	data, err := wai.SearchInWhatsapp()
	if err != nil {
		helper.ResponseError(c, "gagal mendapatkan pencarian", err)
		return
	}
	c.JSON(http.StatusOK, helper.GinJSONResponse(true, "sukses mendapatkan pencarian", data))
}
