module whatsapp-service-ocin

go 1.16

require (
	github.com/Rhymen/go-whatsapp v0.1.1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.4
	github.com/joho/godotenv v1.3.0
	github.com/skip2/go-qrcode v0.0.0-20190110000554-dc11ecdae0a9
)

replace github.com/Rhymen/go-whatsapp => github.com/miqbalrr/go-whatsapp v0.1.3
