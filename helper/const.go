package helper

import "time"

// auth config
const JwtAlgorithm = "HS256"

// connection config
const TimeoutConnection = 20 * time.Second
const ShortClientName = "WA Ocin"
const LongClientName = "Whatsapp Service Ocin"
const ClientVersion = "0.0.1"

// session config
const DirSession = "session"
const DirTemp = "temp"

// sending config
const RemoteJID = "@s.whatsapp.net"
const RemoteGroupJID = "@g.us"

// response collection
const ErrorWAConnectionMsg = "failed to connect whatsapp"

// inventio config
const InventioGroupId = "6281216166848-1590673871"
const InventioAdminNumber = "6289678003287"
