package helper

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func GinJSONResponse(success bool, message string, data interface{}) gin.H {
	return gin.H{
		"success": success,
		"message": message,
		"data":    data,
	}
}

func ResponseSuccess(c *gin.Context, message string, data interface{}) {
	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"message": message,
		"data":    data,
	})
}

func ResponseError(c *gin.Context, message string, data interface{}) {
	c.JSON(http.StatusInternalServerError, gin.H{
		"success": false,
		"message": message,
		"data":    data,
	})
}

func ResponseUnauthorized(c *gin.Context, message string, data interface{}) {
	c.JSON(http.StatusUnauthorized, gin.H{
		"success": false,
		"message": message,
		"data":    data,
	})
}
