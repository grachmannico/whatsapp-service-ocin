package helper

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"net/http"
	"os"
	"strings"
)

func JwtDecoder(tokenString string, secret string) error {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if jwt.GetSigningMethod(JwtAlgorithm) != token.Method {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(secret), nil
	})

	if err != nil {
		return err
	}
	if !token.Valid {
		return errors.New("token invalid")
	}
	return nil
}

func JwtMiddlewareAuth(c *gin.Context) {
	godotenv.Load()

	var err error
	headerAuthorization := c.Request.Header.Get("Authorization")
	secret := os.Getenv("JWT_AUTH_SECRET")

	if headerAuthorization == "" {
		c.JSON(http.StatusUnauthorized, GinJSONResponse(false, "Unauthorized", "missing authorization"))
		c.Abort()
		return
	}

	headerAuthorization = strings.Replace(headerAuthorization, "Bearer ", "", -1)

	err = JwtDecoder(headerAuthorization, secret)
	if err != nil {
		c.JSON(http.StatusUnauthorized, GinJSONResponse(false, "Unauthorized", err))
		c.Abort()
		return
	}
}
