package helper

import (
	"encoding/gob"
	"fmt"
	"github.com/Rhymen/go-whatsapp"
	"github.com/Rhymen/go-whatsapp/binary"
	"github.com/joho/godotenv"
	"github.com/skip2/go-qrcode"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"
)

type WaInstance struct {
	connection *whatsapp.Conn
	version    version
}

type version struct {
	major int
	minor int
	patch int
}

var QrPath string

func NewWaInstance() *WaInstance {
	return &WaInstance{}
}

func (wa *WaInstance) Connect() error {
	wa.setInitVersion()

	wac, err := whatsapp.NewConnWithOptions(&whatsapp.Options{
		Timeout:         TimeoutConnection,
		ShortClientName: ShortClientName,
		LongClientName:  LongClientName,
		ClientVersion:   ClientVersion,
	})
	if err != nil {
		return err
	}

	wac.AddHandler(&waHandler{wac, uint64(time.Now().Unix())})
	wac.SetClientVersion(wa.version.major, wa.version.minor, wa.version.patch)
	wa.connection = wac
	return nil
}

func (wa *WaInstance) setInitVersion() {
	godotenv.Load()

	major, _ := strconv.Atoi(os.Getenv("WA_VERSION_MAJOR"))
	minor, _ := strconv.Atoi(os.Getenv("WA_VERSION_MINOR"))
	patch, _ := strconv.Atoi(os.Getenv("WA_VERSION_PATCH"))

	wa.version = version{major: major, minor: minor, patch: patch}
}

func (wa *WaInstance) TestConnection() (bool, error) {
	var err error

	if err = wa.Login(); err != nil {
		return false, err
	}

	return wa.connection.AdminTest()
}

func (wa *WaInstance) Login() error {
	session, err := readSession()
	if err == nil {
		session, err = wa.connection.RestoreWithSession(session)
		if err != nil {
			return fmt.Errorf("restoring session failed: %v", err)
		}
	} else {
		qr := make(chan string)

		go func() {
			suffix := strconv.FormatInt(time.Now().Unix(), 10)
			QrPath = DirTemp + "/qr" + suffix + ".png"
			err = qrcode.WriteFile(<-qr, qrcode.Medium, 256, QrPath)
		}()

		session, err = wa.connection.Login(qr)
		if err != nil {
			_ = wa.DeleteQrCode(QrPath)
			return fmt.Errorf("error during login: %v", err)
		}
	}

	if err = writeSession(session); err != nil {
		_ = wa.DeleteQrCode(QrPath)
		return fmt.Errorf("error saving session: %v", err)
	}

	_ = wa.DeleteQrCode(QrPath)
	return nil
}

func (wa *WaInstance) SendMessage(waNumber string, message string) (string, error) {
	var msgId string
	var err error

	if err = wa.Login(); err != nil {
		return "", err
	}

	text := whatsapp.TextMessage{
		Info: whatsapp.MessageInfo{
			RemoteJid: waNumber + RemoteJID,
		},
		Text: message,
	}

	msgId, err = wa.connection.Send(text)
	if err != nil {
		return "", nil
	}

	return msgId, nil
}

func (wa *WaInstance) SendInvoiceMessage(waNumber string, message string) (string, error) {
	var msgId string
	var img *os.File
	var thumbnail []byte
	var err error

	if err = wa.Login(); err != nil {
		return "", err
	}

	img, err = os.Open("asset/qris.png")
	if err != nil {
		return "", err
	}

	thumbnail, err = ioutil.ReadFile("asset/qris.png")
	if err != nil {
		return "", err
	}

	msg := whatsapp.ImageMessage{
		Info: whatsapp.MessageInfo{
			RemoteJid: waNumber + RemoteJID,
		},
		Type:      "image/jpeg",
		Caption:   message,
		Thumbnail: thumbnail,
		Content:   img,
	}

	msgId, err = wa.connection.Send(msg)
	if err != nil {
		return "", nil
	}

	return msgId, nil
}

func (wa *WaInstance) SendGroupMessage(groupId string, message string) (string, error) {
	var msgId string
	var err error

	if err = wa.Login(); err != nil {
		return "", err
	}

	text := whatsapp.TextMessage{
		Info: whatsapp.MessageInfo{
			RemoteJid: groupId + RemoteGroupJID, //6281216166848-1590673871
		},
		Text: message,
	}

	msgId, err = wa.connection.Send(text)
	if err != nil {
		return "", nil
	}

	return msgId, nil
}

func (wa *WaInstance) GetQrCode() string {
	return strings.Replace(QrPath, "temp/", "", 1)
}

func (wa *WaInstance) DeleteQrCode(qrPath string) error {
	if err := os.Remove(qrPath); err != nil {
		return err
	}
	return nil
}

func (wa *WaInstance) GetInfoWhatsapp() (*whatsapp.Info, error) {
	var err error

	if err = wa.Login(); err != nil {
		return nil, err
	}

	return wa.connection.Info, nil
}

func (wa *WaInstance) SearchInWhatsapp() (*binary.Node, error) {
	var res *binary.Node
	var err error

	if err = wa.Login(); err != nil {
		return nil, err
	}
	res, err = wa.connection.Search("inventio", 1, 1)

	return res, err
}

func readSession() (whatsapp.Session, error) {
	session := whatsapp.Session{}

	file, err := os.Open(DirSession + "/whatsappSession.gob")
	if err != nil {
		return session, err
	}
	defer file.Close()

	decoder := gob.NewDecoder(file)
	if err = decoder.Decode(&session); err != nil {
		return session, err
	}

	return session, nil
}

func writeSession(session whatsapp.Session) error {
	file, err := os.Create(DirSession + "/whatsappSession.gob")
	if err != nil {
		return err
	}
	defer file.Close()

	encoder := gob.NewEncoder(file)
	if err = encoder.Encode(session); err != nil {
		return err
	}

	return nil
}
