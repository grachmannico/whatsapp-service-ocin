package main

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"net/http"
	"os"
	"whatsapp-service-ocin/controller"
	"whatsapp-service-ocin/helper"
)

func main() {
	godotenv.Load()

	r := gin.Default()
	r.Use(helper.JwtMiddlewareAuth)
	r.Use(cors.Default())

	// GET
	r.GET("/", func(c *gin.Context) { c.String(http.StatusOK, "Wuttt") })
	r.GET("/get_qrcode_url", controller.GetQrCodeUrl)
	r.GET("/get_info", controller.GetInfoWhatsapp)
	r.GET("/serach", controller.SearchInWhatsapp)

	// POST
	r.POST("/login", controller.Login)
	r.POST("/test_connection", controller.TestConnection)
	r.POST("/send_message", controller.SendMessage)
	r.POST("/send_invoice", controller.SendInvoice)
	r.POST("/send_to_inventio_group", controller.SendToInventioGroup)

	// Static
	r.Static("/img", helper.DirTemp+helper.QrPath)

	r.Run(":" + os.Getenv("GIN_PORT"))
}
